import React, { useRef, useState, useEffect } from "react";

export interface Person {
  firstName: string;
  lastName: string;
}

export interface TextFieldProps {
  text: string;
  ok?: boolean;
  i?: number;
  fn?: (bob: string) => string;
  obj?: {
    f1: string;
  };
  person: Person;
  handleChange?: (e: React.ChangeEvent<HTMLInputElement>) => void | undefined;
}

export const TextField: React.FC<TextFieldProps> = (TextFieldProps) => {
  // //hook useState domyslnie ustawia nam typy
  // const [count, setCount] = useState(5);
  // //jesli chcemy w przyszlosci, aby setCount mogło ustawiać rowniez np na typ null i undefined:
  // const [count2, setCount2] = useState<number | null |undefined >(5)

  // setCount2(null);
  // setCount2(undefined);

  // const [count3, setCount3] = useState<{text: string}>({text:"hello"})

  // const [count4, setCount4] = useState<Person>({firstName:"Aga", lastName: "Kunicka"})

  const inputRef = useRef<HTMLInputElement>(null);
  const divRef = useRef<HTMLDivElement>(null);
  const nameRef= useRef<HTMLInputElement>(null);
  const lastnameRef = useRef<HTMLInputElement>(null);

  const [name, setName] = useState<string>("");
  const [lastname, setLastame] = useState<string>("");
  const [ok, setOk] = useState<boolean>(false)
  const [message, setMessage] = useState<string>("");

  useEffect(() => {
      setMessage("Dzięki za subskrypcję");
      console.log(nameRef.current);
      return () => {
        setMessage("Twoja subskrybcja została anulowana");
        setOk(!ok);
      }
  }, [ok])



  

  return (
    <div ref={divRef}>
        <form>
      <input ref={inputRef} onChange={TextFieldProps.handleChange} />
      <input
      ref={nameRef}
        placeholder="imię"
        onChange={(e) => {
          console.log(e.target.value);
          setName(e.target.value);
        }}
      />
      <input
      ref={lastnameRef}
        onChange={(e) => {console.log(e.target.value);
        setLastame(e.target.value)}}
        placeholder="nazwisko"
      />
      <input type="checkbox" id="subscribeNews" name="subscribe" value="newsletter"/>
    <label>Subscribe to newsletter?</label>
    <button type="submit" onClick={e=>{
        setOk(!ok);
        e.preventDefault()}}>Subscribe</button>
  
      </form>
      <p>
        {name} {lastname} witamy w drużynie subskybentów!
        {message}
      </p>

    </div>
  );
};
